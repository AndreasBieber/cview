This document lists architectural details of cview.

# Widgets always use `sync.RWMutex`

See [#30](https://gitlab.com/tslocum/cview/-/issues/30).
