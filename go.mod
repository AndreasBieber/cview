module gitlab.com/tslocum/cview

go 1.12

require (
	github.com/gdamore/tcell/v2 v2.0.0-dev.0.20200908121250-0c5e1e1720f1
	github.com/lucasb-eyer/go-colorful v1.0.3
	github.com/mattn/go-runewidth v0.0.9
	github.com/rivo/uniseg v0.1.0
	gitlab.com/tslocum/cbind v0.1.2-0.20200826214515-b5f2c6a8711a
	golang.org/x/sys v0.0.0-20200908134130-d2e65c121b96 // indirect
)
